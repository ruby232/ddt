# Instrucciones para configurar ddt-up
* Crear una carpeta para el proyecto y una carpeta "config" dentro de esta.
* Copiar el fichero ddt-upp.
* Copiar la carpeta "files" desde "sites/default/" dentro de  "config" y el fichero settings.php.
* Darle permisos de ejecución al fichero y de escritura al carpeta por usuario que ejecuta el script.  
    ** OJO. El usuario del servidor web tiene que tener permiso de escritura a la carpeta files **

### Quedando de esta forma:
```
carpeta_projecto
│   ddt-up
└───config
    | files
    | setttings.php
```

# Ejecutar el script
```bash
./ddt-upp
```
Despues de ejecutado debe quedar asi

```
carpeta_projecto
│   ddt-up
|   logfile.log
|   release.xml
|   active(Enlace a la version activa) 
|   BD(Salvas de la vase de datos)
|   versions
|   | pcd-v0.0.5 (Version activa)
└───config
    | files
    | setttings.php
```

#Dependencias
## notify-osd
sudo apt-get install notify-osd libnotify-bin

#### Por ultimo crear un enlace simbolico desde la carpeta de la web a la carpeta active.

## Este script debe ejecutarce periodicamente, se puede usar "Crontab" o "Supervisor" 