# idea tomada de http://www.cubicrace.com/2016/03/efficient-logging-mechnism-in-shell.html

SCRIPT_LOG=/var/log/ddt/ddt.log
touch $SCRIPT_LOG

function SCRIPTENTRY(){
    timeAndDate=`date`
    script_name=`basename "$0"`
    echo "$FUNCNAME: $script_name" >> $SCRIPT_LOG
}

function SCRIPTEXIT(){
    script_name=`basename "$0"`
    echo "$FUNCNAME: $script_name" >> $SCRIPT_LOG
}

function INFO(){
    MSG "INFO" "$1" 
}

function ERROR(){
    MSG "ERROR" $1 
}

function MSG(){
    local type="$1"
    local msg="$2"
    local tstamp=`date`
    echo $msg
    echo -e "[$tstamp] [$type]\t$msg" >> $SCRIPT_LOG
}