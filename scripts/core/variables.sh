#!/bin/sh

# Definning constants
CAT=$(which cat)
CHMOD=$(which chmod)
CHOWN=$(which chown)
CP=$(which cp)
RSYNC=$(which rsync)
DATE=$(which date)
DATETIME=$($DATE '+%Y%m%d%H%M')
DRUSH=$(which drush)
if [[ "$OSTYPE" == "darwin"* ]]; then
    DRUSH="/opt/drush"
fi
ECHO=$(which echo)
LN=$(which ln)
LS=$(which ls)
MKDIR=$(which mkdir)
MKTEMP=$(which mktemp)
MV=$(which mv)
RM=$(which rm)
RMDIR=$(which rmdir)
SUDO=$(which sudo)
SED=$(which sed)

#ToDo averiguar como saber si estoy en alpine
OSTYPE=linux-gnu

if [[ "$OSTYPE" == "linux-gnu" ]]; then
    TEMP_BUILD=$($MKTEMP -d)
elif [[ "$OSTYPE" == "darwin"* ]]; then
     TEMP_BUILD=$($MKTEMP -d /tmp/ddt.XXXX )
else
    $ECHO "Solo soporta LINUX y MAC"
    exit 1
fi
        
# Definning variables
PROFILE_REGEX="s/([a-zA-Z0-9\_]+)\.info(.yml)*/\1/p"
ASK=true
ASKP=""
DESTINATION="active"
INIT=false
LOCALE=false
URLPROFILE=""
PROJECT=""
DVERSION=7
SUPASS="Admin123"

#Datos del sitio
ACNAME="admin"
ACPASS="Admin123"
DOMAIN="http://localhost"
SITEMAIL="admin@gmail.com"

if [ -z "$DBDRIVER" ];then DBDRIVER="pgsql"; fi
if [ -z "$DBHOST" ];then DBHOST="localhost"; fi
if [ -z "$DBUSER" ];then DBUSER="root"; fi
if [ -z "$DBPASSWD" ];then DBPASSWD="Admin123"; fi
if [ -z "$DBPREFIX" ];then DBPREFIX=""; fi
if [ -z "$DEFAULT_VALUES" ];then DEFAULT_VALUES=""; fi
if [ -z "$DRUSH_DEBUG" ];then DRUSH_DEBUG=""; fi

DOCKER=false
if grep docker /proc/1/cgroup -qa; then
    DOCKER=true
fi

# Positioning the script at the base of the structure
ROOTDIR=$(pwd)
DESTINATION=$ROOTDIR/active

ONLY_INSTALL=false
#Mode de despliege, [prod, dev, test]
MODE="dev"
