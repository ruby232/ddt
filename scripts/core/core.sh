#!/bin/sh

#Incluir el fichero de las constantes y variables por defecto
if [ -f /usr/share/ddt/scripts/core/variables.sh ]; then
        . /usr/share/ddt/scripts/core/variables.sh
    else
    echo "!! No se encuentra el fichero /usr/share/ddt/variables.sh !!!!"
    exit 1
fi

default_values(){
if [ ! -f $ROOTDIR/profile/default_values.ini ]; then
  $ECHO "Sin valores por defecto."  
else

    while read -r value;do
    DEFAULT_VALUES+=" $value"
    done < "$ROOTDIR/profile/default_values.ini"  
fi
}

sass_compile(){
    GULP_EXIST="NO"
    gulp_exec
     if [ "${GULP_EXIST}" == "NO" ];  then
            compass_compile
     fi             
}

compass_compile(){
$ECHO "==>> Compile SASS"
if [ -f config.rb ]; then
    #Comprobar si esta instalado el compass
    program="compass"
    condition=$(which $program 2>/dev/null | grep -v "not found" | wc -l)
    if [ $condition -eq 0 ] ; then
        echo "$program is not installed"
    else
        $program compile
    fi
fi 
}

gulp_exec(){
$ECHO "==>> Gulp Execute"
if [ -f gulpfile.js ]; then
    #Comprobar si esta instalado el npm
    program="npm"
    condition=$(which $program 2>/dev/null | grep -v "not found" | wc -l)
    if [ $condition -eq 0 ] ; then
        echo "$program is not installed"
    else
        $program install --unsafe-perm
        gulp
        GULP_EXIST="YES"
    fi
fi 
}

project_name(){
    if [ "${URLPROFILE}" == "" ];  then
            #obtener el nombre del projecto a partir del nombre del profile
            if [[ "$OSTYPE" == "linux-gnu" ]]; then
                PROJECT=`ls profile | sed -rn $(echo $PROFILE_REGEX)`            
           elif [[ "$OSTYPE" == "darwin"* ]]; then
                PROJECT=`ls profile | sed -En $(echo $PROFILE_REGEX)`                
            fi
            
    else
            #obtener el nombre del proyecto de la direccion del git
    
            #para los que esten en el gitd7 ej: git@gitd7.datys.cu:pcd.git
            PROJECT=$(echo $URLPROFILE |cut -d"/" -f2|cut -d"." -f1);
            
            if [ "${PROJECT}" == "" ];then
                #Para TFS ejemplo  https://dc-tfs-app.datys.cu/tfs/DPM/PortalDelConocimiento/_git/pcd
                PROJECT=$(echo $URLPROFILE | rev | cut -d "/" -f 1 | rev);
            fi
    fi 
    SITENAME=$PROJECT
}

# Ejecutar el script pust-build.sh si existe
#variables
## $DESTINATION
post_create_distribution(){
    current_dir=$(pwd)
    if [ -f profile/composer.json ]; then
        $ECHO "     ==>> Instalando dependencias de composer"
        cp profile/composer.json $DESTINATION/composer.json
        cd $DESTINATION
        rm -f composer.lock
        composer install
    fi
    
    if [ -f profile/post-build.sh ]; then
        $ECHO "     ==>> Ejecute ./post-buid.sh"
        cp profile/post-build.sh $DESTINATION/post-build.sh
        cd $DESTINATION
        chmod +x post-build.sh
        . ./post-build.sh
        rm post-build.sh
    fi
    cd $current_dir
}

#Crear la distribucoin a partir del perfil
# Variables usadas
## $PROJECT
## $TEMP_BUILD
## $TEMP_BUILD
## $DESTINATION
create_distribution(){

    CPARG="-ar"
    if [[ "$OSTYPE" == "darwin"* ]]; then
        CPARG="-a"
    fi
        
    #limpiar la carpeta active o crearla si no existe
    if [ -d $DESTINATION ]; then
        $ECHO   "==>> Limpiando carpeta active"
        if $DOCKER; then
            rm -rf $DESTINATION
        else
            $ECHO $SUPASS | $SUDO -S rm -rf $DESTINATION
        fi
    fi

    $ECHO   "==>> Creando carpeta active"
    $MKDIR -p $DESTINATION
    $MKDIR -p $DESTINATION/cache
    if $DOCKER; then
        $CHMOD 777 $DESTINATION/cache
    else
        $ECHO $SUPASS | $SUDO -S $CHMOD 777 $DESTINATION/cache
    fi

     YML=""
    if [ ! -f profile/drupal-org.make ]; then
        YML=".yml"
        DVERSION=8
    fi
    
    if [ ! -f profile/drupal-org.make$YML ]; then
        $ECHO "[error] Run this script expecting ./profile/ directory."
        exit 1
    fi
    
    $RMDIR $TEMP_BUILD
    
    #compilando sass
    cd profile
    sass_compile
    cd ..

    MAKE_OPTIONS=""
    if [ "${DRUSH_SOURCE}" != "" ];then
        MAKE_OPTIONS="--make-update-default-url=${DRUSH_SOURCE}"
    fi

    # Build the profile.
    $ECHO "==>> Building the profile ..."
    $DRUSH make $DRUSH_DEBUG --no-core $MAKE_OPTIONS profile/$PROJECT.make$YML tmp || { $ECHO "!!!! FAILED Building the profile !!!!!"; exit 1; }
    
    # Build the distribution and copy the profile in place.
    $ECHO "==>> Building the distribution..."
    $DRUSH make $DRUSH_DEBUG $MAKE_OPTIONS profile/drupal-org-core.make$YML $TEMP_BUILD || { $ECHO "!!!! FAILED Building the distribution !!!!!"; exit 1; }
    
    $ECHO "==>> Moving to destination..."
        
    $MKDIR -p $TEMP_BUILD/profiles/$PROJECT

    if [[ "$DVERSION" == "7" ]]; then
        #Drupal 7
        $CP $CPARG  tmp/sites/. $TEMP_BUILD/sites
    fi
    
    if [[ "$DVERSION" == "8" ]]; then
            #Drupal 8
            if [ -d tmp/modules ]; then
                    $CP $CPARG tmp/modules/. $TEMP_BUILD/modules
            fi
            if [ -d tmp/themes ]; then
                    $CP $CPARG tmp/themes/. $TEMP_BUILD/themes
            fi
            if [ -d tmp/libraries ]; then
                if [ ! -d $TEMP_BUILD/libraries ]; then
                       $MKDIR -p $TEMP_BUILD/libraries
                fi
                $CP $CPARG tmp/libraries/. $TEMP_BUILD/libraries
            fi
            $MKDIR -p $TEMP_BUILD/sites/default/files/translations/
            $CHMOD 777 $TEMP_BUILD/sites/default/files/translations/
            if [ -d profile/translations ]; then
                $CP $CPARG profile/translations/* $TEMP_BUILD/sites/default/files/translations
            fi
      fi
      
    $RSYNC -a --exclude=.git profile/ $TEMP_BUILD/profiles/$PROJECT
                
    if [ -e profile/favicon.ico ]; then
        $CP $CPARG  profile/favicon.ico $TEMP_BUILD/favicon.ico
    fi
     
    $CP $CPARG $TEMP_BUILD/. $DESTINATION
    post_create_distribution
           
    $RM -rf tmp
    $RM -rf $TEMP_BUILD
}

#Crear los enlaces symbolicos necesarios
create_distribution_links(){
    # Create symblic links
    $ECHO "==>> Creating symbolic links..."

    $RM -rf $DESTINATION/profiles/$PROJECT
    #Enlazarlo al de la carpeta ROOTDIR del profile  al release activo
    $LN -s $ROOTDIR/profile $DESTINATION/profiles/$PROJECT

    $MKDIR -p $ROOTDIR/config/files/
    $CHMOD 777 $ROOTDIR/config/files/
    
    # Copiar l acarpeta file si existe
    if [ -d "${DESTINATION}/sites/default/files" ]; then
        $CP -rf $DESTINATION/sites/default/files/* $ROOTDIR/config/files
        $RM -r $DESTINATION/sites/default/files/
    fi

   if $DOCKER; then
        $LN -s $ROOTDIR/config/files $DESTINATION/sites/default/
    else
        $ECHO $SUPASS | $SUDO -S $LN -s $DESTINATION $WEB_PATH
    fi
}

debug_option(){
    
    #Opciones de desarrollo
    if [[ "$DVERSION" == "8" ]]; then
            if [[ "$MODE" == "dev" ]]; then
            $ECHO "==>> Configurando opciones de Debug"
              if [ ! -f "$ROOTDIR/config/settings.local.php" ]; then
                    $CP /usr/share/ddt/files/settings.local.php $ROOTDIR/config/settings.local.php
              fi  
              
              if [ !  -f "$ROOTDIR/config/development.services.yml" ]; then
                    $CP /usr/share/ddt/files/development.services.yml $ROOTDIR/config/development.services.yml
              fi  
              
             if [ -f "$ROOTDIR/active/sites/development.services.yml" ]; then
                    $RM $ROOTDIR/active/sites/development.services.yml
              fi       
              
              $LN -s $ROOTDIR/config/settings.local.php $ROOTDIR/active/sites/default/ 
              $LN -s $ROOTDIR/config/development.services.yml $ROOTDIR/active/sites/      
              
              LINE_DEBUG="$(grep -n 'if (file_exists($app_root' $ROOTDIR/config/settings.php | cut -f1 -d:)"
              LINE_DEBUG_END=$(($LINE_DEBUG + 2))
              
              $SED -i "${LINE_DEBUG},${LINE_DEBUG_END} s/^#//" $ROOTDIR/config/settings.php 
              
            fi
    fi
}

notify_message(){
    #Compromar si notify-osd esta instalado
    if [ -d "/usr/share/notify-osd" ]; then
        notify-send "DDT Install" \
                                   "${message}" \
                                    -i /usr/share/ddt/files/ddt.svg
    fi
}

# insert/update hosts entry
#variables 
## HOST_NAME
add_entry_host(){
    ip_address="127.0.0.1"
    # find existing instances in the host file and save the line numbers
    matches_in_hosts="$(grep -n $HOST_NAME /etc/hosts | cut -f1 -d:)"
    host_entry="${ip_address} ${HOST_NAME}"

    if [ ! -z "$matches_in_hosts" ]; then
        $ECHO "====>> La entrada ya existe."
        # iterate over the line numbers on which matches were found
        #while read -r line_number; do
            # replace the text of each line with the desired host entry
           # sudo sed -i '' "${line_number}s/.*/${host_entry} /" /etc/hosts
        #done <<< "$matches_in_hosts"
    else
        $ECHO "==> Adding new hosts entry."
        if $DOCKER; then
            $ECHO "$host_entry" | tee -a /etc/hosts > /dev/null
        else
            $ECHO "$host_entry" | sudo tee -a /etc/hosts > /dev/null
        fi
    fi
}
