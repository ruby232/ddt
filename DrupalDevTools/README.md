Drupal DATYS Tools - Update Production
========================================

Scripts for help in Drupal sites deploy.

Introduction
------------
Scripts for help developers in the process of deploy a web site in Drupal.

Components
----------
For execute command *ddt-upp* go to the project folder.

Installation
------------
Install the package *DrupalDatysTools.deb* making double click in it and push the buttom Install Package.

# ddt-upp
ddt-upp it is main script, based in deploy.sh ( https://github.com/DavidHernandez/profile-boilerplate )
Clone the web site from the server project and it will make to ready for install.

## Use
First time you need create the main directory with the name of the project.

Inside of the directory write in console
```sh
ddt-upp
```
This command:
+ Clone the repository project
+ Create directory structure
+ Use the last release of the distribution
+ Update the Distribution
+ Create three symbolic link:
  + {project_name}/active
  + {project_name}/active/sites/default/files
  + {project_name}/active/sites/default/settings.php

Project Folder
--------------
The main project folder should look like
```sh
  - {project}
    - active/
    - BD/
    - config/
    - versions/
    - logfile.log
    - release_server.xml
```

Active make the installation project in the web.

Credits and thanks
------------------

* Inspired in philosophy present for David Hernadez y Ramon Vilar in
[Arquitectura de desarrollo en proyectos Drupal](http://2015.drupalcamp.es/node/142), [Drupal Architecture Workshop](http://2015.drupalcamp.es/node/66),[Drupal projects architecture](http://davidhernandez.info/blog/drupal-projects-architecture “Drupal projects architecture | David Hernandez)

* Thanks to [David Hernandez](http://davidhernandez.info) always willing to help.
