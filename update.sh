#!/bin/sh

ROOTDIR=$(pwd)
#Borrando todos los enlaces del ddt
rm -f /usr/bin/ddt*
ln -s $ROOTDIR/bin/ddt /usr/bin/ddt 
ln -s $ROOTDIR/bin/ddt-upp /usr/bin/ddt-upp
ln -s $ROOTDIR/bin/ddtp /usr/bin/ddtp

#Borrando la carpeta shared
rm -rf /usr/share/ddt

mkdir -p /usr/share/ddt/

ln -s $ROOTDIR/files /usr/share/ddt/files
ln -s $ROOTDIR/scripts /usr/share/ddt/ 
