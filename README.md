Drupal DevOps Tools
========================================

Scripts for help in Drupal develop.

Introduction
------------
Scripts for help developers in the process of construct web in Drupal only in code with profiles.
Starting with scripts in:
[profile-boilerplate](https://github.com/DavidHernandez/profile-boilerplate)
[profile-demo](https://github.com/DavidHernandez/profile-demo)

Components
----------
For execute command *ddt* go to the project folder
Others commands execute from *release active* folder 

# ddt
ddt it is main script, based in deploy.sh ( https://github.com/DavidHernandez/profile-boilerplate )
Creates a new release of the project and enables it.

## Use
First time you need clone de profile
```sh
ddt -c url-git-profile
```
This command:
+ Create foulder with name of the profile
+ Clone profile
+ Create directory structure
+ Create first realese o de distribution 
+ Install Distribution
+ Create symbolic link in /var/www/name-profile

In case you have created a project only execute
```sh
ddt
```
 
Use -y for ejecute without quietions.

# ddt-cf 
### Create features
Create, update and actife features (taxonomy menu_custom filter field_base field_instance node views_view).

# ddt-pu
### Update proyect
+ Update git profile repository
+ Download contribuid modules
+ Update database
+ Revert all features
+ Clear all caches

# ddt-cr
### Create release
Create distribution for install site.
Delete files specific in **/usr/share/ddt/delete_profile.txt** and **/usr/share/ddt/delete.txt**

##Use
```sh
ddt-cr -d destination-direcory
```
If *destination-directory* not specificate it will be created in release folder.

# ddt-release
### Release
Use for install profile in CI server.

Credits and thanks
------------------

* Inspired in philosophy present for David Hernadez y Ramon Vilar in 
[Arquitectura de desarrollo en proyectos Drupal](http://2015.drupalcamp.es/node/142), [Drupal Architecture Workshop](http://2015.drupalcamp.es/node/66),[Drupal projects architecture](http://davidhernandez.info/blog/drupal-projects-architecture “Drupal projects architecture | David Hernandez)

* Thanks to [David Hernandez](http://davidhernandez.info) always willing to help.



