#!/usr/bin/env bash

#define parameters which are passed in.
SERVER_NAME=$1
ROOT=$2
PHP_VERSION=$3

# PHP 7 socket location.
FASTCGI_PASS="unix:/var/run/php/php7.0-fpm.sock;"
if $DOCKER;then
    FASTCGI_PASS="upstream;"
fi
if [ "$PHP_VERSION" == "5"  ];then
    # PHP 5 socket location.
    FASTCGI_PASS="unix:/var/run/php5-fpm.sock;"
    if $DOCKER;then
        FASTCGI_PASS="upstream;"
    fi
fi
        
cat  << EOF
server {
    server_name $SERVER_NAME;
    root $ROOT;

    location = /favicon.ico {
        log_not_found off;
        access_log off;
    }

    location = /robots.txt {
        allow all;
        log_not_found off;
        access_log off;
    }

    location ~ \..*/.*\.php$ {
        return 403;
    }

    location ~ ^/sites/.*/private/ {
        return 403;
    }

    # Allow "Well-Known URIs" as per RFC 5785
    location ~* ^/.well-known/ {
        allow all;
    }

    location ~ (^|/)\. {
        return 403;
    }

    location / {
        try_files \$uri /index.php?\$query_string; 
    }

    location @rewrite {
        rewrite ^/(.*)$ /index.php?q=\$1;
    }

    location ~ /vendor/.*\.php$ {
        deny all;
        return 404;
    }

    location ~ '\.php$|^/update.php' {
        fastcgi_split_path_info ^(.+?\.php)(|/.*)$;
        include fastcgi_params;
        fastcgi_param HTTP_PROXY "";
        fastcgi_param SCRIPT_FILENAME \$document_root\$fastcgi_script_name;
        fastcgi_param PATH_INFO \$fastcgi_path_info;
        fastcgi_intercept_errors on;
        fastcgi_pass $FASTCGI_PASS

    }

    location ~ ^/sites/.*/files/styles/ { 
        try_files \$uri @rewrite;
    }

    location ~ ^/system/files/ { 
        try_files \$uri /index.php?\$query_string;
    }

    location ~* \.(js|css|png|jpg|jpeg|gif|ico)$ {
        expires max;
        log_not_found off;
    }
}
EOF
